package ru.t1consulting.nkolesnik.tm.service;

import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public User remove(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return userRepository.create(login, password);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(login)) throw new EmailAlreadyExistException();
        final User user = userRepository.create(login, password);
        user.setEmail(email);
        return userRepository.add(user);
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userRepository.create(login, password);
        user.setRole(role);
        return userRepository.add(user);
    }

    @Override
    public User create(String login, String password, String email, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.create(login, password);
        user.setEmail(email);
        user.setRole(role);
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findById(email);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String middleName, final String lastName) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        final User user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User removeById(String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        final User user = userRepository.findById(id);
        return userRepository.remove(user);
    }

    @Override
    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        return userRepository.remove(user);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public boolean isLoginExist(String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

    @Override
    public int getSize() {
        return userRepository.findAll().size();
    }

}
