package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
