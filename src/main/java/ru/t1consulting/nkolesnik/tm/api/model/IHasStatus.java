package ru.t1consulting.nkolesnik.tm.api.model;

import ru.t1consulting.nkolesnik.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
