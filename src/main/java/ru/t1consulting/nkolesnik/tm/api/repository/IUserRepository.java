package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User User);

    User create(String login, String password);

    User remove(User User);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    void clear();

}
