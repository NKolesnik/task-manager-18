package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.model.User;

public interface IAuthService {

    void registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
