package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

import java.util.List;

public interface IUserService {

    User add(User user);

    User remove(User user);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password, String email, Role role);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String middleName, String lastName);

    User removeById(String id);

    User removeByLogin(String login);

    void clear();

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    int getSize();

}
