package ru.t1consulting.nkolesnik.tm.exception.entity;

public class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
