package ru.t1consulting.nkolesnik.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
