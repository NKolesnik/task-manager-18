package ru.t1consulting.nkolesnik.tm.repository;

import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User remove(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isLoginExist(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public boolean isEmailExist(String email) {
        for (User user : users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

    @Override
    public void clear() {
        users.clear();
    }

}
